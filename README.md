# README #

### What is this repository for? ###

The OpenWaterDielectricConstant repository contains an open source C++ implementation for computing the static dielectric
constant of water as a function of temperature and water density. 

The implementation is based on Release IAPWS R8-97 ("Release on the Static Dielectric Constant of Ordinary Water Substance 
For Temperatures from 238 K to 873 K and Pressures up to 1000 MPa" of the International Association for the Properties of 
Water and Steam, available under http://www.iapws.org/relguide/Dielec.html.

If you need computation as a function of temperature and pressure you need an independent library for an equation of state
of water (ideally the IAPWS95 equation of state), in order to be able to compute density at temperature and pressure.

### How do I get set up? ###

The formulation is implementated in a single C++ class named WaterDielectricConstant (files "WaterDielectricConstant.h" and
"WaterDielectricConstant.cpp"). Instantiating an object of type WaterDielectricConstant requires providing temperature in 
Kelvin and density kg/m3 as constructore argument, e.g.

	double tKelvin{298.15};
	double rho_kgm3{1000.0};

	epsilon WaterDielectricConstant(tKelvin,rho_kgm3);

Besides this custom constructor, the class has a single public member function ValueAt_T_and_Rho() that requires no parameters.
As the constructor arguments of WaterDielectricConstant are const references to T and rho as they exist in the scope in which
it was instantiated, WaterDielectricConstant automatically "knows" their current values, i.e.

cout << epsilon.ValueAt_T_and_Rho() << endl;

will always return the static dielectric constant for the current T and rho. The use is illustrated by the main.cpp file.

WaterDielectricConstant has no dependency on other files except the standard C++ libraries <cmath> and <array>.

### Contribution guidelines ###

Contributions in terms of writing tests, code review, etc. are highly welcome.
* Other guidelines

### Who do I talk to? ###

Thomas Driesner at ETH Zurich; use thomas.driesner@erdw.ethz.ch for contact.